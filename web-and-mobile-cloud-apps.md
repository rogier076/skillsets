
# Ideal candidate profile for Javascript based Modern mobile & web cloud applications

We'd be looking for a developer that is a pro in one of these fields, and preferably more of these field.
Someone who has experience in all fields would be best. Otherwise someone who is a pro at two fields. 
A fullstack dev that has all the backend skills and all the frontend skills except the mobile apps, would be a better candidate than a frontend dev with mobile skills. 


## General skills

 - Experience with scrum / agile
 - Teamwork 
 - Project based development
 - Scalable applications

## Frontend-dev skills

**Required (needs to be very experienced):**

 - TypeScript (especially in combination with React and Vue)
 - HTML (5)
 - CSS (3)
 - Javascript / ECMA Script 
 - Rest API's 
 
And at least one of the following: 

 - React (JS)
 - Angular 2+
 - Vue (JS)       

**Highly desired:**

 - Advanced state management with libraries such as MobX, Redux, NgRX
 - GraphQL
 - Persistent state management (using local storage or cookies)
 - NodeJS
 - Server side rendering (SSR) (e.g. NextJS, NuxtJS, Gatsby, Universal)
 - Service workers 
 - Websockets
 - MVVM and MVC models
  - Writing unit tests (e.g. Jest, Jasmine, Mocha)
 - Swagger

**Great to have:**

 - Component based libraries such as Material design frameworks ( Material UI, Angular MDC, Vue MDC etc)
 - Webpack, Gulp, Polyfill
 - Bootstrap 4
 - GPS- and navigation libraries
 - jQuery (note, this should never be the language of choice but just to support legacy code)

**For mobile apps:**

 - Push notifications with firebase and APN
 - App store and play store configuration

One or more of the following:

 - React Native 
 - Ionic 
 - Cordova
 - Nativescript
 - Capacitor

**note:** these are platform independent (javascript) frameworks. Xamarin is .NET framework. Native iOS apps are written in swift and android apps in kotlin or java. They do not match this profile.

## Backend-dev skills

**Required (needs to be very experienced):**

- NodeJS
- Express and (or) Serverless framwork
- REST API's and microservices
- NoSQL / Document Database
- SQL / Relational Database

At least one of the following NoSQL

 - MongoDB
 - Redis
 - DynamoDB
 - Elasticsearch
 - Couchbase

At least one of the following SQL:

 - PostgreSQL
 - MySQL (MariaDB)
 - MS SQL 
 - OracleDB

**Highly desired** 

 - Experience in another server side language, such as Python, C# .NET, GoLang, Java or PHP
 - State/session management across concurrent processes 
 - SOAP - API's 
 - Reverse proxies
 - GraphQL (Apollo server)
 - Websockets 
 - Message Queue's e.g. RabbitMQ, Amazon SQS
 - MVVM and MVC models
  - Writing unit tests (e.g. Jest, Jasmine, Mocha)
 - Swagger

## Cloud & Devops skills

**At least one of the following (preferably certified):**

 - AWS (Amazon Web Services)
 - Azure (Microsoft)
 - Google Cloud Services

**Note:** If not certified, I'd suggest they get certified at their own expense before getting hired. The exam is around €135 and training info can be found online or bought for $15. I was able to get certified for AWS Solution Architect Associate (not the entry certificate) within 2 weeks. 

**Highly desired:**

 - Docker
 - Cloudformation
 - CI/CD Pipelines
 - Kafka
 - Sonarcloud
 - Linux (Debian, Ubuntu, CentOS, Redhat)
 - Serverless (Lambda, Functions)
 - Kubernetes

 **will see to it to add testing frameworks as well**
